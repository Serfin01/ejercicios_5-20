#include <stdio.h> //12. Escriba un programa que solicite 10 números al usuario y muestre la suma y el producto de todos ellos. Use el bucle while.

int main ()
{
    int sum = 0;
    int product = 1;
    int i = 0;
    int num;
    
    while (i<10)
    {
        printf ("Dame el numero %i : \n", i+1);        
        scanf ("%i", &num);
        
        getchar ();
        product *= num;
        sum += num;
        i++;
    }
    
    system("cls");
    printf ("suma= %d\nproducto = %d\n",sum, product);
    getchar();
    return 0;
}