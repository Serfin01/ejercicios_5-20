#include <stdio.h>
/*
20. Escribe un programa de calculadora. El flujo del programa debe ser el siguiente:
1) Solicite un número (flotante)
2) Solicite la operación deseada: '+', '-', '*', '/'
3) Solicite otro número (flotante)
4) Mostrar operación y resultados
5) Ir al paso 2
*/

int main ()
{
    int num1;
    int num2;
    int resul;
    int opcion;
    
    do
    {
        printf ("Introduce numero: \n");
        scanf ("%i", &num1);
        getchar();
        
        do
        {
            printf ("Introduce operacion: 1=+ // 2=- // 3=* // 4=/ // 5=Salir\n");
            scanf ("%i", &opcion);
            getchar();
            
            if (opcion > 5 || opcion <= 0)
            {
                printf ("Esa opción no es valida, se le volvera a pedir que introduzca un numero\n");
            }
        } while (opcion > 5 || opcion <= 0);
        
        if (opcion != 5)
        {
            printf ("Introduce 2o numero: \n");
            scanf ("%i", &num2);
            getchar();
        }
        
        switch (opcion)
        {
            case 1: resul = num1 + num2;
                break;
            case 2: resul = num1 - num2;
                break;
            case 3: resul = num1 * num2;
                break;
            case 4: resul = num1 / num2;
                break;
        }  
        
         if (opcion != 5)
        {
            printf ("Resultado = %i\n", resul);
        }
    }while (opcion != 5);
    
   getchar();
   return 0;
}