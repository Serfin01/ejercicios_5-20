#include <stdio.h>//17. Escribe un programa que imprima cinco números aleatorios, como una lotería.

int main ()
{
    int numeros = 5;
    time_t t;
    
    srand((unsigned) time (&t));
    
    printf ("Numeros elegidos: ");
    
    for (int i = 0; i<numeros; i++)
    {
        printf ("%d ", rand()%50);
    }
    
   getchar();
   return 0;
}