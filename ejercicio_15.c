#include <stdio.h>//15. Escriba un programa que solicite 1 número al usuario y muestre su valor binario equivalente. Puede mostrar el binario en orden invertido.

int main ()
{
    int resto;
    int num;
    
    do{
    printf ("numero: \n");        
    scanf ("%i", &num);
    getchar ();
    
    if (num < 0)
        {
            printf ("Valor no valido\n");
        }
    
    } while (num < 0);
    
    printf ("binario: ");
    
    while (num > 0)
    {
        resto = num % 2;
        num /= 2;
        printf ("%d", resto);            
    }
    printf ("\n"); 
    
    getchar ();
    return 0;
}