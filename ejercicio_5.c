#include <stdio.h> //05. Escriba un programa que solicite el radio de un círculo y encuentre su diámetro, circunferencia y área.

int main()
{
    float radio;
    float tmp;
     
    printf("/*********************************/\n");
    printf("/**********Dame un radio**********/\n");
    printf("/*********************************/\n");
    
    scanf("%f", &radio);
    
    tmp = radio + radio;
    getchar();
    system("cls");
    printf("Diametro = %f \n", tmp);
    
    tmp = 3,14 * radio * 2;
    getchar();
    system("cls");
    printf("Circunferencia = %f \n", tmp);
    
    tmp = 3,14 * radio * radio;
    getchar();
    system("cls");
    printf("Area = %f \n", tmp);
    
    getchar();
    
    return 0;
}