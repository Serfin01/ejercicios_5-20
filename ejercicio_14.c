#include <stdio.h>//14. Escriba un programa que solicite una secuencia de números al usuario y muestre la media de todos ellos. El proceso se detiene solo cuando se introduce el número 0.

int main ()
{
    int sum = 0;
    int media;
    int num;
    int i = 0;
    
    do 
    {
        printf ("Dame el numero %i : \n", i+1);
        scanf ("%i", &num);
        
        getchar();
        if (num != 0)
            {
                sum += num;
                 i++;
            }
    } while (num != 0);
    
    media = sum / i;
    printf ("media = %d\n", media);
    getchar();
    
    return 0;
}