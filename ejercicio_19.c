#include <stdio.h>//19. Escribe una función para calcular la potencia de un valor. La declaración de la función debe ser: int RaiseToPower (int value, int power);

int RaiseToPower(int value, int power)
{
    int resul = 1;
    
    for (int i=0; i<power; i++)
    {
        resul *= value;
    }
    
    return resul;
}

int main ()
{
    int num;
    int poten;
    int resul;
    
    
    printf ("Numeros a calcular: \n");
    scanf ("%i", &num);
    
    getchar();
    printf ("Introduce la potencia: \n");
    scanf ("%i", &poten);
    
    getchar();
    resul = RaiseToPower (num, poten);
    
    
    printf ("El resultado es: %i\n", resul);
    
    
    getchar();
    return 0;
}