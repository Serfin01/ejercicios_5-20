#include <stdio.h>//16. Escribe un programa que muestre los primeros 100 números primos.

int main ()
{
    int cont = 0;
    int div = 0;
    int i;
    
    printf ("Primeros 100 numero primos:");
    
    for (int j = 2; cont < 100; j++)
    {
        div = 0;
        i = 2;
        while (i <= j && div == 0)
        {
            if (j%i == 0 && j==i)
            {
                printf (" %i ", j);
                cont++;
            }
            else if (j%i == 0)
            {
                div = 1;
            }
            i++;
        }
             
     }
        
    getchar ();
    return 0;
}