#include <stdio.h> //9 Escriba un programa que solicite segundos y conviértalo en minutos, horas y días.

int main()
{
    float sec;
    float min;
    float hora;  
    float dia;
    
    printf("/******************************/\n");
    printf("/**********Dame segundos*******/\n");
    printf("/******************************/\n");
    
    scanf("%f", &sec);
    
    getchar();
    min = sec/60;
    system("cls");
    printf("minutos = %f \n", min);
    
    getchar();
    hora = min/60;
    system("cls");
    printf("horas = %f \n", hora);
    
    getchar();
    dia = hora/24;
    system("cls");
    printf("dias = %f \n", dia);
    
    getchar();
    
    return 0;
}